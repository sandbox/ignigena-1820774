<iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo $playlist['videos'][1]['id']; ?>?enablejsapi=1&playerapiid=playlist_block_youtube_player" frameborder="0" allowfullscreen id="playlist_block_youtube_player">
</iframe>
<script>
  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('playlist_block_youtube_player', {
      events: {
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerStateChange(event) {
    jQuery('#playlist_block_youtube_player').attr('class','player-state-'+event.data);
  }
  
  function youTubePlayerPlay(id) {
    player.cueVideoById(id);
    player.playVideo();
  }
</script>
<?php
  $header = array('Thumbnail', 'Title', 'Likes', 'Uploaded');
  $rows = array();
  
  // Built the block using the data available in $playlist_data.
  foreach ($playlist['videos'] as $videos) {
    $thumbnail_image = array(
      'path' => $videos['thumbnail'],
      'alt' => $videos['title'],
      'title' => $videos['title'],
      'attributes' => NULL,
    );
    $thumbnail_image = theme_image($thumbnail_image);
    $link_address = 'javascript:youTubePlayerPlay(\'' . $videos['id'] . '\');';
    $rows[] = array(
      '<div class="thumbnail-trim">' . l($thumbnail_image, $link_address, array(
        'attributes' => array('target' => 'playlist_block_youtube_player'),
        'html' => TRUE,
        'external' => TRUE,
      )) . '</div>',
      l($videos['title'], $link_address, array(
        'attributes' => array('target' => 'playlist_block_youtube_player'),
        'external' => TRUE,
      )),
      $videos['likes'],
      format_date($videos['uploaded'], variable_get('block_youtube_playlist_date_format')),
    );
  }
      
  print theme_table(array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => NULL,
    'caption' => NULL,
    'colgroups' => NULL,
    'sticky' => TRUE,
    'empty' => t('No videos found in playlist.'),
  ));
?>
